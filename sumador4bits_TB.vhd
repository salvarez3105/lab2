----------------------------------------------------------------------------------
-- Course: Laboratorio Dise�o Digital
-- Engineer: Francinie Rodr�guez Sol�rzano
-- 
-- Create Date: 23.02.2019 00:35:12
-- Design Name: 
-- Module Name: sumador4bits_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sumador4bits_TB is

end sumador4bits_TB;

architecture Behavioral of sumador4bits_TB is
    -- Declaraci�n del componente para la unidad bajo prueba (UUT)
    COMPONENT sumador4bits
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         Cin : IN  std_logic;
         F : OUT  std_logic_vector(3 downto 0);
         Cout : OUT  std_logic
        );
    END COMPONENT;
 
   --Entradas
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal Cin : std_logic := '0';
   --Salidas
   signal F : std_logic_vector(3 downto 0);
   signal Cout : std_logic;
 
BEGIN
   -- Instalar la unidad bajo prueba uut
   uut: sumador4bits PORT MAP (
          A => A,
          B => B,
          Cin => Cin,
          F => F,
          Cout => Cout
        );
 
   stim_proc: process -- Proceso estimulo
begin

    for i in std_logic range '0' to '1' loop
         Cin <= i;
         for j in 0 to 15 loop
            A <= std_logic_vector(to_unsigned(j, 4));
            for k in 0 to 15 loop
               B <= std_logic_vector(to_unsigned(k, 4));
               wait for 10 ns;
            end loop;
         end loop;
      end loop;
 
   end process;
END;
