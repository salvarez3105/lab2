`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Course: Taller de dise�o digital 
// Engineer: samuel   �lvarez Rojas 
// 
// Create Date: 19.03.2019 13:30:18
// Design Name: modulo para el shift de la ALU
// Module Name: modo_shifter
// Project Name: ALU parametrizable 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module modo_shifter #(parameter ancho=3)(
    input logic [ancho-1:0] a,
    input logic [ancho-1:0] b,
    input alu_flag_in,opcode_1,opcode_2,
    output logic [ancho-1:0] y,
    output logic c,zero,overflow,negativo
    );
logic [1:0] select;
logic [ancho-1:0] ysll;
logic [ancho-1:0] ysrl; 
logic csll,csrl;
// estaba en el main 
modo_srl #(3) shift_srl (a,b,alu_flag_in,ysrl,csrl);
modulo_sll #(3) shift_sll (a,b,alu_flag_in,ysll,csll);

assign select = {opcode_2,opcode_1};

mux_4a1parametrizable #(3) salidas_shift (ysll,ysrl,3'b0,3'b0,select,y);
mux_4a1parametrizable #(1) salidas_c (csll,csrl,1'b0,1'b0,select,c); 

always_comb 
begin 
case(y)
0 : zero = 1'b1;
default zero = 1'b0;
endcase  
end 

assign overflow = 1'b0;
assign negativo = 1'b0;
         
endmodule
