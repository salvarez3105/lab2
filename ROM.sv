`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.05.2019 04:22:39
// Design Name: 
// Module Name: ROM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM(

input wire clk,
input wire [7:0] addr,
output reg [11:0] datos

    );
    
    reg [3:0]direccion;
    
    always @(posedge clk)
    direccion<=addr;
    
    always @ *
  
    case( addr) 
     8'h00: datos = 12'h000;//color 0
     8'h01: datos = 12'h000;
     8'h02: datos = 12'h000;
     8'h03: datos = 12'h000;
     8'h04: datos = 12'h000;
     8'h05: datos = 12'h000;
     8'h06: datos = 12'h000;
     8'h07: datos = 12'h000;
     8'h08: datos = 12'h000;
     
    endcase
    
    
endmodule
