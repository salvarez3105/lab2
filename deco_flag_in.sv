`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 22:58:03
// Design Name: 
// Module Name: deco_flag_in
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module deco_flag_in(
    input logic flag_in,
    input opcode_2,
    input opcode_1,
    output logic out1,
    output  logic out2
    );
always_comb
begin 
case ({flag_in,opcode_2,opcode_1})
0 : begin out1 = 1'b0; out2 = 1'b1;end  
1 : begin out1 = 1'b0; out2 = 1'b0;end
2 : begin out1 = 1'bx; out2 = 1'bx;end
3 : begin out1 = 1'b1; out2 = 1'b0;end
4 : begin out1 = 1'b1; out2 = 1'b0;end
5 : begin out1 = 1'bx; out2 = 1'bx;end
6 : begin out1 = 1'b1; out2 = 1'b0;end
7 : begin out1 = 1'b1; out2 = 1'b0;end
endcase
end   
endmodule
