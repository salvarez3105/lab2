`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Course: Tller de Dise�o Digital  
// Engineer: Samuel �lvarez Rojas 
// 
// Create Date: 13.03.2019 18:16:05
// Design Name: modo L�gico
// Module Name: m_logico
// Project Name: ALU parametrizable
// Target Devices: Nexys A7
// Tool Versions: 
// Description: Modo l�gico principal de la ALU 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module m_logico #(parameter ancho = 3)(
    input [ancho-1:0]a,
    input [ancho-1:0]b,
    input wire op_code1,op_code2,
    input wire alu_flag_in,
    output [ancho-1:0]y,
    output logic negativo,zero,c,overflow
    );  
    
 logic select1,select3,sal_enable_op,sal_enable_xor ;
 logic [1:0] select2;
 deco_flag_in  flag_in (alu_flag_in,op_code2,op_code1,sal_enable_op,sal_enable_xor);
 assign select1 = sal_enable_op;
 assign select2 = {op_code2,op_code1};
 logic [ancho-1:0] a_negado;  
 logic [ancho-1:0] b_negado;
 logic [ancho-1:0] salida_a,salida_b,salidas;
 logic  [ancho-1:0] op_nand,op_and,op_xor;
 logic [1:0] select4;
 assign a_negado = ~(a);
 assign b_negado = ~(b);
 
mux_2a1parametrizable #(3) mux_a (a,a_negado,select1,salida_a);
mux_2a1parametrizable #(3) mux_b (b,b_negado,select1,salida_b);
assign op_nand = ~(salida_a & salida_b);
assign op_and=~op_nand;
mux_4a1parametrizable #(3) mux_operaciones (op_nand,op_and,a_negado,b_negado,select2,salidas);
assign op_xor = a ^ b;
assign select3 = sal_enable_xor;
mux_2a1parametrizable #(3) mux_op2 (salidas,op_xor,select3,y);
mux_4a1parametrizable #(1) mux_negativo (1'b0,1'b0,1'b1,1'b1,select2,negativo);

always_comb
begin 
case (y)
0 : begin zero = 1'b1;end
1 : begin zero = 1'b0;end
2 : begin zero = 1'b1;end
3 : begin zero = 1'b0;end
4 : begin zero = 1'b1;end
5 : begin zero = 1'b0;end
6 : begin zero = 1'b1;end
7 : begin zero = 1'b0;end
default: zero = 1'b0;
endcase
end 

assign c = 1'b0;
assign overflow = 1'b0;
      
endmodule
