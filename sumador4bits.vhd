----------------------------------------------------------------------------------
-- Course: Laboratorio Diseño Digital
-- Engineer: Francinie Rodríguez Solórzano
-- 
-- Create Date: 23.02.2019 00:30:25
-- Design Name: 
-- Module Name: sumador4bits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sumador4bits is
    Port ( A    : in   STD_LOGIC_VECTOR (3 downto 0);
           B    : in   STD_LOGIC_VECTOR (3 downto 0);
           Cin  : in   STD_LOGIC;
           F    : out  STD_LOGIC_VECTOR (3 downto 0);
           Cout : out  STD_LOGIC);
end sumador4bits;

architecture Behavioral of sumador4bits is
    COMPONENT sumador --declaración del componente
    PORT(
        a    : IN  std_logic;
        b    : IN  std_logic;
        cin  : IN  std_logic;
        f    : OUT std_logic;
        cout : OUT std_logic
        );
    END COMPONENT;
   --señal intermedia para conectar el carry entre sumadores completos
   signal C : std_logic_vector(2 downto 0);
begin
    
    sumador0: sumador PORT MAP(  --instanciación Sumador0
        a    => A(0),
        b    => B(0),
        cin  => Cin,
        f    => F(0),
        cout => C(0));
 
   sumador1: sumador PORT MAP(  --instanciación Sumador0
        a    => A(1),
        b    => B(1),
        cin  => C(0),
        f    => F(1),
        cout => C(1));
 
   sumador2: sumador PORT MAP(  --instanciación Sumador0
        a    => A(2),
        b    => B(2),
        cin  => C(1),
        f    => F(2),
        cout => C(2));
 
   sumador3: sumador PORT MAP(  --instanciación Sumador0
        a    => A(3),
        b    => B(3),
        cin  => C(2),
        f    => F(3),
        cout => Cout);

end Behavioral;
