`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2019 01:46:53
// Design Name: 
// Module Name: Generador_pixeles
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Generador_pixeles#(
parameter colorD=12,
          addr=10,
          key_color=0
          )
(
    input logic clk,
    input logic [10:0] x,y, //coordenadas x y Y
    input logic [10:0] xo,yo,//origen del sprite
    input logic [4:0] control //control del sprite
        );
endmodule
