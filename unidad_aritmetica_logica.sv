`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.03.2019 22:44:28
// Design Name: 
// Module Name: unidad_aritmetica_logica
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module unidad_aritmetica_logica #(parameter ancho =3)(
    input [ancho-1:0]a,
    input [ancho-1:0]b,
    input alu_flag_in,
    input [3:0] opcode,
    input CLK100MHZ,
    output c,
    output zero,
    output negativo,
    output overflow,
    output logic [7:0]AN,
    output [6:0]SEG
    );
logic [ancho-1:0] y0,y1,y2,y3;
logic c0,c1,c2;
logic zero0,zero1,zero2;
logic negativo0,negativo1,negativo2;
logic overflow0,overflow1,overflow2;

logic [18:0] contador=0;
logic clk2=1;
always @(posedge CLK100MHZ)
        begin
        contador<=contador+1;
        if (contador==500_000)
            begin
            contador<=0;
            clk2 <=~clk2;
            end
        end 
        
        always @(clk2)
            begin
                 if (clk2)
                    begin
                    AN = 8'b11111110;
                    end 
                else 
                    begin
                    AN=8'b11111111; 
                    end 
            end    
m_aritmetico #(3) modo_a (a,b,opcode[0],opcode[1],alu_flag_in,y0,negativo0,c0,zero0,overflow0);
m_logico  #(3) modo_b (a,b,opcode[0],opcode[1],alu_flag_in,y1,negativo1,zero1,c1,overflow1);
modo_shifter #(3) modo_c (a,b,alu_flag_in,opcode[0],opcode[1],y2,c2,zero2,overflow2,negativo2);

mux_4a1parametrizable #(3) mux_salidas (y0,y1,y2,3'b0,{opcode[3],opcode[2]},y3);

mux_4a1parametrizable #(1) mux_c (c0,c1,c2,1'b0,{opcode[3],opcode[2]},c);
mux_4a1parametrizable #(1) mux_zero (zero0,zero1,zero2,1'b0,{opcode[3],opcode[2]},zero);
mux_4a1parametrizable #(1) mux_negativo (negativo0,negativo1,negativo2,1'b0,{opcode[3],opcode[2]},negativo);
mux_4a1parametrizable #(1) mux_overflow (overflow0,overflow1,overflow2,1'b0,{opcode[3],opcode[2]},overflow);

deco_7segments deco_7segmentos ({1'b0,y3},SEG);


endmodule
