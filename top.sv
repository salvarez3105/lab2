`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.05.2019 23:07:14
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk,
    input reset,
    output logic [11:0] rgb,
    output logic hsync,
    output logic vsync
    );
logic [11:0] entrada=12'h00f;
logic [10:0] x,y;
logic [10:0] x0=150,y0=111;  
logic [11:0] rgb_sal;

sprite figura (.clk(clk),.reset(reset),.x(x),.y(y),.x0(x0),.y0(y0),.bypass_reg1(1'b1),.si_rgb(entrada),.so_rgb(rgb_sal));
vga_sync_demo sincronizador (.clk(clk),.reset(reset),
.vga_si_rgb(rgb_sal),.hsync(hsync),.vsync(vsync),
.rgb(rgb),

.hc(x),.vc(y));
 
endmodule
