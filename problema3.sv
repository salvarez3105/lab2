`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.03.2019 22:53:50
// Design Name: 
// Module Name: problema3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module problema3(
input wire enable,
    input wire reset,
    input wire reset_anodos,
    input wire clk,
    output logic [7:0]anodos,
    output logic [6:0]segmentos 

    );
    wire reset_debounce;
wire enable_debounce; 
logic clk2, clk3;
logic [2:0] cuenta;

div_clk  clok_div (.clk_in(clk),.clk_out(clk2));
        always @(clk2,reset_anodos)
            begin
                if ((clk2)|~reset_anodos)
                    anodos =8'b11111110;
                else 
                    anodos=8'b11111111;
            end
         
 debounce reset_debo (.clock(clk),.IN(reset),.OUT(reset_debounce));
 debounce enable_debo (.clock(clk),.IN(enable),.OUT(enable_debounce));
 counter_1 #(3) c(.clk(enable_debounce),.reset(reset_debounce),.cuenta(cuenta));  //.contador(enable_debounce) ,
 deco_7segments bloqueDeco(.datos({1'b0,cuenta}),.segmentos(segmentos)); 
 
endmodule
