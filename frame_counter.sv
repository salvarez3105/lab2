`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.05.2019 23:01:19
// Design Name: 
// Module Name: frame_counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module frame_counter #(parameter HMAX = 640,VMAX = 480)(
    input clk,
    input reset,
    input inc,
    input sync_clr,
    output logic [10:0]cuenta_h,
    output logic [10:0]cuenta_v
    );
logic [10:0] hc_reg=0, hc_next=0;
logic [10:0] vc_reg=0, vc_next=0; 

// horizontal and vertical pixel counters 
   // register
   always_ff @(posedge clk, posedge reset)
      if (reset) begin
         vc_reg <= 0;
         hc_reg <= 0;
      end   
      else if (sync_clr)  begin
         vc_reg <= 0;
         hc_reg <= 0;
      end 
      else begin 
         vc_reg <= vc_next;
         hc_reg <= hc_next;
      end
      // next-state logic of horizontal counter
   always_comb
      if (inc) 
         if (hc_reg == (HMAX - 1))
            hc_next = 0;
         else
            hc_next = hc_reg + 1;
      else
         hc_next = hc_reg;
   
   // next-state logic of vertical counter
   always_comb
      if (inc && (hc_reg == (HMAX - 1)))
         if (vc_reg == (VMAX - 1))
            vc_next = 0;
         else
            vc_next = vc_reg + 1;
      else
         vc_next = vc_reg;

assign cuenta_h=hc_reg;  
assign cuenta_v=vc_reg;  
endmodule
