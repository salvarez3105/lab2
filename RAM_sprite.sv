`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.05.2019 00:53:46
// Design Name: 
// Module Name: RAM_sprite
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RAM_sprite 
#(parameter addr_ancho=10, //  # bits direcciones
            dato_ancho=3  // # bits de color
  )
(
    input logic clk,
    input logic we,
    input logic [addr_ancho-1:0] addr_r,
    input logic [addr_ancho-1:0] addr_w,
    input logic [dato_ancho-1:0] dato_in,
    output logic [dato_ancho-1:0] dato_out
    );
    
    logic [dato_ancho-1:0] ram [0:2**addr_ancho-1];
    logic [dato_ancho-1:0] dato_reg;
    
    initial
     $readmemb("",ram);//archivo con datos de sprite
     
     always_ff @(posedge clk)
     begin
        if(we)
            ram[addr_w]<=dato_in;
        dato_reg <= ram[addr_r];
     end
    assign dato_out = dato_reg;
endmodule
