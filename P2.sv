`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Course:Taller de Dise�o de Sistemas Digitales 
// Engineer: Samuel �lvarez Rojas - Francinie Rodriguez Solorzano
// 
// Create Date: 04.03.2019 22:24:10
// Design Name: Main problema 2
// Module Name: P2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module P2(
    input wire [7:0] SW,
    input wire CLK100MHZ,
    output logic [6:0] SEG,
    output logic [7:0] AN
    );
logic [18:0] contador=0;
    logic clk2=1;
    wire [3:0] suma_1;
    wire [3:0] suma_2;
    wire cin=0;
    logic cout;
    
    always @(posedge CLK100MHZ)
        begin
        contador<=contador+1;
        if (contador==500_000)
            begin
            contador<=0;
            clk2 <=~clk2;
            end
        end 
        
        always @(clk2)
            begin
                 if (clk2)
                    begin
                    AN = 8'b11111110;
                    end 
                else
                    begin
                    AN=8'b11111101; 
                    end 
            end    
             
            
    sumador4bits bloqueSumador (.A(SW[3:0]),.B(SW[7:4]),.Cin(cin),.F(suma_1),.Cout(cout));
    deco_7segments bloqueDeco (.datos(suma_2),.segmentos(SEG));
    mux_displays bloqueMux (.clk(clk2),.unidades(suma_1),.decenas({3'b000,cout}),.datos(suma_2));
endmodule
