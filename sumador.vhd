----------------------------------------------------------------------------------
-- Course: Laboratorio Dise�o Digital
-- Engineer: Francinie Rodr�guez Sol�rzano 
-- 
-- Create Date: 23.02.2019 00:03:01
-- Design Name: 
-- Module Name: sumador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sumador is
    Port (
      a    : in STD_LOGIC;
      b    : in STD_LOGIC;
      cin  : in STD_LOGIC;
      f    : out STD_LOGIC;
      cout : out STD_LOGIC);
end sumador;

architecture Behavioral of sumador is
begin

   f <= a xor b xor cin;
   cout <= (a and b) or (cin and (a xor b));

end Behavioral;
