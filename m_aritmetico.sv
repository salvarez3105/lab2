`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Course: Taller de Dise�o Digital 
// Engineer: Francinie Rodriguez Solorzano
// 
// Create Date: 13.03.2019 22:55:20
// Design Name: Modo Aritmetico 
// Module Name: m_aritmetico
// Project Name: ALU parametrizable 
// Target Devices: Nexys A7
// Tool Versions: 
// Description: Modulo principal Arimetico para la ALU 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module m_aritmetico #(parameter ancho = 3)(
    input [ancho-1:0] a,
    input [ancho-1:0] b,
    input op_code1,op_code2,alu_flag_in,
    output [ancho-1:0] y,
    output logic negativo,
    output logic c,
    output logic zero,
    output logic overflow
    );

logic select1,select2;
logic [1:0] select3;
logic sal_cin,sal_operando;
logic [ancho-1:0] sal_ab1;
logic [ancho-1:0] sal_ab2; 
logic  select4;
logic [ancho-1:0] salida_select_compl2;
logic [1:0] resta;

deco_cin  flag_in (op_code2,alu_flag_in,sal_cin,sal_operando);

mux_2a1parametrizable #(3) mux_b (a,b,sal_operando,sal_ab1);
mux_2a1parametrizable #(3) mux_operando (a,sal_ab1,select2,sal_ab2);
logic ent1=1'b0;
logic ent2=1'b1;
mux_2a1parametrizable  mux_habilitador (ent1,ent2,select1,select2);
assign select1=op_code2;
logic [ancho-1:0] b_compl2;

compl_2 #(3) b_complemento (b,b_compl2);

logic [ancho-1:0] incremento_1=3'b001;
logic [ancho-1:0] decremento_1=3'b001;
logic [ancho-1:0] operando_compl2;
logic [ancho-1:0] salida_select1;
logic [ancho-1:0] salida_select2;
logic msb;
logic nega;
logic cond;
compl_2 #(3) op_complemento (decremento_1,operando_compl2);

mux_4a1parametrizable  #(3) operaciones_aritemeticas (b,b_compl2,incremento_1,operando_compl2,{op_code2,op_code1},salida_select1);

carry_lookahead_adder #(3) sumador (sal_ab2,salida_select1,sal_cin,msb,salida_select2);
compl_2 #(3) s_complemento (salida_select2,salida_select_compl2);

assign resta = {op_code2,op_code1};
assign nega = ((b>a))? 1'b1: 1'b0; 
assign cond = decremento_1>a ^ decremento_1>b;
always_comb 
begin 
case({resta,cond,nega})
0 : negativo = 1'b0;
1 : negativo = 1'b0;
2 : negativo = 1'b0;
3 : negativo = 1'b0;
4 : negativo = 1'b0;
5 : negativo = 1'b1;
6 : negativo = 1'b0;
7 : negativo = 1'b1;
8 : negativo = 1'b0;
9 : negativo = 1'b0;
10 : negativo = 1'b0;
11 : negativo = 1'b0;
12 : negativo = 1'b0;
13 : negativo = 1'b0;
14 : negativo = 1'b1;
15 : negativo = 1'b1;
default negativo = 1'b0;
endcase  
end 


 
assign c = msb;

assign overflow = (({msb,y}== 4'b1110) | ({msb,y}== 4'b1111) ) ? 1'b1 :1'b0;

always_comb 
begin 
case(y)
0 : zero = 1'b1;
default zero = 1'b0;
endcase  
end 

mux_2a1parametrizable #(3) mux_salida (salida_select2[ancho-1:0],salida_select_compl2[ancho-1:0],negativo,y);

endmodule
