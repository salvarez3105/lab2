`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2019 03:35:27
// Design Name: 
// Module Name: tiempos_prop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tiempos_prop #(parameter ancho = 32)(
    input [ancho-1:0]a,
    input [ancho-1:0]b,
    input alu_flag_in,
    input [3:0] opcode,
    input CLK100MHZ,
    input reset,
    output c,
    output zero,
    output negativo,
    output overflow, 
    output logic [ancho-1:0]y
    );

logic [3:0] salidas_ff;
//logic [7:0] salida_final;

logic [ancho-1:0] y0,y1,y2,y3;
logic c0,c1,c2;
logic zero0,zero1,zero2;
logic negativo0,negativo1,negativo2;
logic overflow0,overflow1,overflow2;

m_aritmetico #(64) modo_a (a,b,salidas_ff[0],salidas_ff[1],alu_flag_in,y0,negativo0,c0,zero0,overflow0);
m_logico  #(64) modo_b (a,b,salidas_ff[0],salidas_ff[1],alu_flag_in,y1,negativo1,zero1,c1,overflow1);
modo_shifter #(64) modo_c (a,b,alu_flag_in,salidas_ff[0],salidas_ff[1],y2,c2,zero2,overflow2,negativo2);

mux_4a1parametrizable #(64) mux_salidas (y0,y1,y2,3'b0,{salidas_ff[3],salidas_ff[2]},y3);

mux_4a1parametrizable #(1) mux_c (c0,c1,c2,1'b0,{salidas_ff[3],salidas_ff[2]},c);
mux_4a1parametrizable #(1) mux_zero (zero0,zero1,zero2,1'b0,{salidas_ff[3],salidas_ff[2]},zero);
mux_4a1parametrizable #(1) mux_negativo (negativo0,negativo1,negativo2,1'b0,{salidas_ff[3],salidas_ff[2]},negativo);
mux_4a1parametrizable #(1) mux_overflow (overflow0,overflow1,overflow2,1'b0,{salidas_ff[3],salidas_ff[2]},overflow);

d_param_ff #(4) ff_entrada (opcode,CLK100MHZ,reset,salidas_ff);

d_param_ff #(64) ff_salida (y3,CLK100MHZ,reset,y);
 
endmodule
