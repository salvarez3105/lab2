`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.03.2019 17:43:48
// Design Name: 
// Module Name: deco_cin
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module deco_cin(
    input logic a,b,
    output logic out1,
    output logic out2
    );
always_comb
begin
case ({a,b})
0 : begin out1 = 1'b0; out2 = 1'b1; end
1 : begin out1 = 1'b1; out2 = 1'b0; end
2 : begin out1 = 1'b0; out2 = 1'b0; end
3 : begin out1 = 1'b0; out2 = 1'b1; end
endcase 
end  
   
    
endmodule
