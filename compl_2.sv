`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.03.2019 17:09:50
// Design Name: 
// Module Name: compl_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module compl_2 #(parameter ancho=1)(
    input logic [ancho-1:0] num,
    output logic [ancho-1:0] num_compl2
    );

logic [ancho-1:0]compl_1; 

assign compl_1=~num;
assign num_compl2 = compl_1+1'b1;
endmodule
