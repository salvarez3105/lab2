`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.05.2019 20:05:17
// Design Name: 
// Module Name: tesbech_sprite
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tesbech_sprite(

    );
    logic [13:0] addr;
    logic cs;
    logic write;
    logic clk , reset;
    logic [10:0] x, y;
    logic [10:0] x0,y0;
    logic bypass_reg1;
    logic [11:0] si_rgb;
    wire [11:0] so_rgb;
    
    sprite spritetoTest(
    .cs(cs),.write(write),.clk(clk), .reset(reset),
    .x0(x0),.y0(y0),
    .bypass_reg1(bypass_reg1),
    .si_rgb(si_rgb),.so_rgb(so_rgb),.addr(addr)
    );
    always
        begin
        #20 clk=0;
        #20 clk=1;
        end
    initial
        begin
        reset=1;
        cs=0;
        x=11'h8; 
        y=11'h7;
        x0=11'h6;
        y0=11'h5;
        bypass_reg1=0;
        si_rgb=12'hF00;
        #100
        cs=0;
        reset=0;
        x=11'h8; 
        y=11'h7;
        x0=11'h6;
        y0=11'h5;
        bypass_reg1=1;
        si_rgb=12'hF00;
        #100
        reset=0;
        x=11'h9; 
        y=11'h8;
        x0=11'h7;
        y0=11'h6;
        bypass_reg1=0;
        si_rgb=12'hF00;
        #100
        reset=0;
        x=11'h9; 
        y=11'h8;
        x0=11'h7;
        y0=11'h6;
        bypass_reg1=1;
        si_rgb=12'hF00;
        end
    
endmodule
